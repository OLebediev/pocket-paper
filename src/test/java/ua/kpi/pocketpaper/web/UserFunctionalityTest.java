package ua.kpi.pocketpaper.web;

import com.jayway.restassured.response.Response;
import org.eclipse.jetty.http.HttpStatus;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.Before;
import org.junit.Test;
import ua.kpi.pocketpaper.persistence.entity.User;
import ua.kpi.pocketpaper.security.MessageDigestProvider;
import ua.kpi.pocketpaper.util.AbstractFunctionalTest;
import ua.kpi.pocketpaper.web.model.TokenResponse;

import java.util.Arrays;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

public class UserFunctionalityTest extends AbstractFunctionalTest {

    private static final String USERNAME = "test";
    private static final String PASSWORD = "123";
    private static final String USERNAME1 = "test1";
    private static final String PASSWORD1 = "321";

    private static final MessageDigestProvider digest = new MessageDigestProvider();

    private static final User user;

    static {
        user = new User();
        user.setUsername(USERNAME);
        user.setPasswordHash(digest.get().digest(PASSWORD.getBytes()));
    }

    @Before
    public void setupMocks() {
        when(userDao.findByUsername(USERNAME)).thenReturn(user);
    }

    @Test
    public void getAccessTokenAndRequestProtectedResource() {
        Response tokenRestResponse = get("/rest/token/{username}/{password}", USERNAME, PASSWORD);
        assertThat(tokenRestResponse.statusCode()).isEqualTo(HttpStatus.OK_200);

        TokenResponse tokenResponse = tokenRestResponse.as(TokenResponse.class);
        String token = tokenResponse.getToken();
        assertThat(token).isNotEmpty();

        Response userRestResponse = given().header("Authorization", "Bearer " + token).when().get("/rest/article/all");
        assertThat(userRestResponse.statusCode()).isEqualTo(HttpStatus.OK_200);
    }

    @Test
    public void requestAccessTokenUsingInvalidCredentials() {
        Response tokenRestResponse = get("/rest/token/{username}/{password}", "invalid", "invalid");
        assertThat(tokenRestResponse.statusCode()).isEqualTo(HttpStatus.FORBIDDEN_403);
    }

    @Test
    public void requestProtectedResourceUsingInvalidToken() {
        Response userRestResponse = given().header("Authorization", "Bearer invalid_token").when().get("/rest/article/all");
        assertThat(userRestResponse.statusCode()).isEqualTo(HttpStatus.FORBIDDEN_403);
    }

    @Test
    public void signUp() {
        given().param("username", USERNAME1).param("password", PASSWORD1)
                .when().post("/rest/user").then().statusCode(HttpStatus.OK_200);

        verify(userDao).save(argThat(new BaseMatcher<User>() {
            @Override
            public boolean matches(Object o) {
                User user = (User) o;
                return USERNAME1.equals(user.getUsername())
                        && Arrays.equals(digest.get().digest(PASSWORD1.getBytes()), user.getPasswordHash());
            }

            @Override
            public void describeTo(Description description) { }
        }));
    }

    @Test
    public void signUpWithExistingUsername() {
        given().param("username", USERNAME).param("password", PASSWORD1)
                .when().post("/rest/user").then().statusCode(HttpStatus.INTERNAL_SERVER_ERROR_500);
        verify(userDao, never()).save(any(User.class));
    }
}
