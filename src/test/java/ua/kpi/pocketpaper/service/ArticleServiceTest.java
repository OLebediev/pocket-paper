package ua.kpi.pocketpaper.service;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import ua.kpi.pocketpaper.persistence.dao.ArticleDao;
import ua.kpi.pocketpaper.persistence.entity.Article;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class ArticleServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8080);

    private ArticleDao articleDao;
    private ArticleExtractionService articleService;

    @Before
    public void setup() {
        articleDao = mock(ArticleDao.class);
        articleService = new ArticleExtractionServiceImpl(articleDao);
    }

    @Test
    public void createdArticleIsSavedAndReturned() {
        //given
        articleService = spy(articleService);
        doReturn("title").when(articleService).extractTitle(anyString());
        doReturn("body").when(articleService).extractArticle(anyString());
        //when
        Article article = articleService.createArticle("http://url");
        //then
        verify(articleService).extractTitle("http://url");
        verify(articleService).extractArticle("http://url");
        verify(articleDao).save(article);
        assertThat(article.getTitle()).isEqualTo("title");
        assertThat(article.getBody()).isEqualTo("body");
    }

    @Test
    public void titleExtractedSuccessfully() {
        //given
        stubArticleResource();
        //when
        String title = articleService.extractTitle("http://127.0.0.1:8080/sample-article.html");
        //then
        assertThat(title).isEqualTo("Xscape Review | Michael Jackson | Compact Discs | Reviews @ Ultimate-Guitar.Com");
    }

    @Test
    public void articleExtractedSuccessfully() {
        //given
        stubArticleResource();
        //when
        String body = articleService.extractArticle("http://127.0.0.1:8080/sample-article.html");
        //then
        assertThat(body.contains("Michael Jackson was a living legend in the pop music world")).isTrue();
        assertThat(body.contains("I've listened to this album a few times trying")).isTrue();
        assertThat(body.contains("Yes, Michael Jackson is a great vocalist")).isTrue();
        assertThat(body.contains("I have a hard time believing the producers who")).isTrue();
    }

    private void stubArticleResource() {
        stubFor(get(urlEqualTo("/__files/sample-article.html"))
                .willReturn(aResponse().withBodyFile("__files/sample-article.html")));
    }
}
