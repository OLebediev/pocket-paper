package ua.kpi.pocketpaper;

import com.google.inject.Module;
import com.google.inject.servlet.GuiceFilter;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.inject.*;
import ua.kpi.pocketpaper.web.GuiceServletConfig;

import javax.servlet.DispatcherType;
import java.net.InetSocketAddress;
import java.util.EnumSet;

public class Bootstrap {

    private static final Logger logger = LoggerFactory.getLogger(Bootstrap.class);

    public static String JERSEY_HOST = "0.0.0.0";
    public static int JERSEY_PORT = 8000;

    private ApplicationStartedEventListener appStartedListener;

    public Bootstrap() { }

    public Bootstrap(ApplicationStartedEventListener appStartedListener) {
        this.appStartedListener = appStartedListener;
    }

    public void startApplication(Module... guiceModules) {
        Server server = new Server(new InetSocketAddress(JERSEY_HOST, JERSEY_PORT));

        ServletContextHandler servletContextHandler = new ServletContextHandler(server, "/",
                ServletContextHandler.NO_SESSIONS);
        servletContextHandler.addEventListener(new GuiceServletConfig(guiceModules));
        servletContextHandler.addFilter(new FilterHolder(GuiceFilter.class), "/rest/*", EnumSet.of(DispatcherType.REQUEST));
        servletContextHandler.addServlet(DefaultServlet.class, "/*");
        servletContextHandler.setResourceBase(Bootstrap.class.getClassLoader().getResource("web").getFile());

        try {
            logger.info("Starting Jetty");
            server.start();
            if (appStartedListener != null) {
                appStartedListener.started();
            }
            server.join();
        } catch (Exception e) {
            logger.error("Jetty has failed with unexpected error", e);
            throw new RuntimeException("Jetty has failed with unexpected error");
        }
    }

    public static void main(String[] args) {
        new Bootstrap().startApplication(productionModules());
    }

    public static Module[] productionModules() {
        return new Module[] {
                new DaoModule(),
                new ServiceModule(),
                new SecurityModule(),
                new WebModule(),
                new HazelcastModule()
        };
    }

    public static interface ApplicationStartedEventListener {
        void started();
    }
}
