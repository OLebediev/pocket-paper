package ua.kpi.pocketpaper.security;

import com.google.inject.Provider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MessageDigestProvider implements Provider<MessageDigest> {

    @Override
    public MessageDigest get() {
        MessageDigest messageDigest;

        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        return messageDigest;
    }
}
