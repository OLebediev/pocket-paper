package ua.kpi.pocketpaper.security;

import ua.kpi.pocketpaper.persistence.entity.User;

public interface TokenProvider {

    String provide(User user);
}
