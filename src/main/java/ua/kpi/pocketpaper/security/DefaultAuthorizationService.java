package ua.kpi.pocketpaper.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.persistence.dao.UserDao;
import ua.kpi.pocketpaper.persistence.entity.User;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.security.MessageDigest;
import java.util.Arrays;

@Singleton
public class DefaultAuthorizationService implements AuthorizationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizationService.class);

    private TokenProvider tokenProvider;
    private TokenStore tokenStore;
    private UserDao userDao;
    private MessageDigestProvider digestProvider;

    @Inject
    public DefaultAuthorizationService(TokenProvider tokenProvider, TokenStore tokenStore, UserDao userDao,
                                       MessageDigestProvider digestProvider) {
        this.tokenProvider = tokenProvider;
        this.tokenStore = tokenStore;
        this.userDao = userDao;
        this.digestProvider = digestProvider;
    }

    @Override
    public String authorize(String username, String password) throws SecurityException {
        User user = userDao.findByUsername(username);

        if (user == null || !checkPassword(user, password)) {
            logger.error("Invalid user or password");
            throw new SecurityException("Invalid user or password");
        }

        String token = tokenStore.getTokenByUser(user);

        if (token == null) {
            token = tokenProvider.provide(user);
            tokenStore.saveToken(token, user);
        }

        logger.info("User [{}] was authorized", user.getUsername());

        return token;
    }

    @Override
    public User authorize(String token) throws SecurityException {
        User user = tokenStore.getUserByToken(token);
        if (user == null) {
            logger.error("Invalid access token");
            throw new SecurityException("Invalid access token");
        }
        return user;
    }

    @Override
    public boolean checkPassword(User user, String password) {
        MessageDigest messageDigest = digestProvider.get();
        messageDigest.update(password.getBytes());
        return Arrays.equals(user.getPasswordHash(), messageDigest.digest());
    }
}
