package ua.kpi.pocketpaper.security;

import ua.kpi.pocketpaper.persistence.entity.User;

public interface TokenStore {

    void saveToken(String token, User user);

    User getUserByToken(String token);

    String getTokenByUser(User user);
}
