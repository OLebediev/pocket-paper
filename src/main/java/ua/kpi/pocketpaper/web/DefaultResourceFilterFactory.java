package ua.kpi.pocketpaper.web;

import com.google.inject.Provider;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;
import ua.kpi.pocketpaper.security.web.TokenAuthSecurityFilter;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class DefaultResourceFilterFactory implements ResourceFilterFactory {

    private Provider<TokenAuthSecurityFilter> securityFilterProvider;

    @Inject
    public DefaultResourceFilterFactory(Provider<TokenAuthSecurityFilter> securityFilterProvider) {
        this.securityFilterProvider = securityFilterProvider;
    }

    @Override
    public List<ResourceFilter> create(AbstractMethod am) {
        if (am.isAnnotationPresent(PermitAll.class)) {
            return null;
        }

        List<ResourceFilter> filters = new ArrayList<>();
        filters.add(securityFilterProvider.get());
        return filters;
    }
}
