package ua.kpi.pocketpaper.web.resource;

import ua.kpi.pocketpaper.persistence.entity.Article;
import ua.kpi.pocketpaper.persistence.entity.User;
import ua.kpi.pocketpaper.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Path("/rest/article")
public class ArticleResource {

    private UserService userService;

    @Inject
    public ArticleResource(UserService userService) {
        this.userService = userService;
    }

    @POST
    @Path("/{url}")
    public Response saveArticle(@Context SecurityContext context, @PathParam("url") String url) {
        userService.saveArticle((User) context.getUserPrincipal(), url);

        return Response.ok().build();
    }

    @GET
    @Path("/{articleId}")
    public Article getArticle(@PathParam("articleId") String articleId) {
        //todo: verify weather article belongs to user
        return userService.getArticle(articleId);
    }

    @GET
    @Path("/all")
    public List<Article> getArticles(@Context SecurityContext context) {
        return userService.getArticles(((User) context.getUserPrincipal()).getUserId());
    }
}
