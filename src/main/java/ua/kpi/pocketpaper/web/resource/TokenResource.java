package ua.kpi.pocketpaper.web.resource;

import ua.kpi.pocketpaper.security.AuthorizationService;
import ua.kpi.pocketpaper.web.model.TokenResponse;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Produces(MediaType.APPLICATION_JSON)
@Path("/rest")
public class TokenResource {

    private AuthorizationService authorizationService;

    @Inject
    public TokenResource(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @GET
    @PermitAll
    @Path("token/{username}/{password}")
    public TokenResponse generateToken(@PathParam("username") String username, @PathParam("password") String password) {
        String token = authorizationService.authorize(username, password);
        return new TokenResponse(token);
    }
}
