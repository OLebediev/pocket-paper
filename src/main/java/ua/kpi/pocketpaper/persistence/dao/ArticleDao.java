package ua.kpi.pocketpaper.persistence.dao;

import ua.kpi.pocketpaper.persistence.entity.Article;

import java.util.List;

public interface ArticleDao {

    Article findById(Object articleId);

    List<Article> findAllByUserId(Object userId);

    void save(Article article);
    
    void delete(Article article);
    
}
