package ua.kpi.pocketpaper.persistence.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import ua.kpi.pocketpaper.persistence.MongoConnectionProvider;
import ua.kpi.pocketpaper.persistence.entity.User;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Igor
 */
@Singleton
public class UserDaoImpl implements UserDao {

    private MongoConnectionProvider connectionProvider;

    @Inject
    public UserDaoImpl(MongoConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    @Override
    public User findById(Object userId) {
        Datastore ds = connectionProvider.get();
        return ds.createQuery(User.class).field("userId").equal(new ObjectId((String) userId)).get();
    }

    @Override
    public User findByUsername(String username) {
        Datastore ds = connectionProvider.get();
        return ds.createQuery(User.class).field("username").equal(username).get();
    }

    @Override
    public void save(User user) {
        Datastore ds = connectionProvider.get();
        ds.save(user);
    }

    @Override
    public void delete(User user) {
        Datastore ds = connectionProvider.get();
        ds.delete(user);
    }
}
