package ua.kpi.pocketpaper.persistence.dao;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import ua.kpi.pocketpaper.persistence.MongoConnectionProvider;
import ua.kpi.pocketpaper.persistence.entity.Article;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

/**
 * @author Igor
 */
@Singleton
public class ArticleDaoImpl implements ArticleDao {

    private MongoConnectionProvider mongoConnection;

    @Inject
    public ArticleDaoImpl(MongoConnectionProvider connectionProvider) {
        this.mongoConnection = connectionProvider;
    }

    @Override
    public Article findById(Object articleId) {
        Datastore ds = mongoConnection.get();
        Query<Article> query = ds.createQuery(Article.class).field("articleId").equal(new ObjectId((String) articleId));
        return query.get();
    }

    @Override
    public List<Article> findAllByUserId(Object userId) {
        Datastore ds = mongoConnection.get();
        Query<Article> query = ds.createQuery(Article.class).field("userId").equal(userId);
        return query.limit(100).asList();
    }

    @Override
    public void save(Article article) {
        Datastore ds = mongoConnection.get();
        ds.save(article);
    }

    @Override
    public void delete(Article article) {
        Datastore ds = mongoConnection.get();
        ds.delete(article);
    }
}
