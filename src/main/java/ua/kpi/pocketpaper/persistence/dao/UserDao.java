package ua.kpi.pocketpaper.persistence.dao;

import ua.kpi.pocketpaper.persistence.entity.User;

public interface UserDao {

    User findById(Object userId);

    User findByUsername(String username);

    void save(User user);

    void delete(User user);
}
