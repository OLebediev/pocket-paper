package ua.kpi.pocketpaper.persistence.entity;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.io.Serializable;
import java.security.Principal;

@Entity("user")
public class User implements Principal, Serializable {

    @Id
    private ObjectId userId;
    private String username;
    private byte[] passwordHash;

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(byte[] passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public String getName() {
        return getUsername();
    }

    @Override
    public boolean equals(Object obj) {
        User another = (User) obj;
        return another != null && another.username != null && another.username.equals(username);
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", username=" + username + ", passwordHash=" + passwordHash + '}';
    }
}
