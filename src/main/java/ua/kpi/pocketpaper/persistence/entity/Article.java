package ua.kpi.pocketpaper.persistence.entity;

import org.bson.types.ObjectId;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import ua.kpi.pocketpaper.persistence.ObjectIdJsonSerializer;

@Entity("article")
public class Article {

    @Id
    @JsonSerialize(using = ObjectIdJsonSerializer.class)
    private ObjectId articleId;
    private ObjectId userId;
    private String title;
    private String body;

    public Article() {
    }

    public ObjectId getArticleId() {
        return articleId;
    }

    public void setArticleId(ObjectId articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public ObjectId getUserId() {
        return userId;
    }

    public void setUserId(ObjectId userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Article{" + "id=" + articleId + ", title=" + title + ", body=" + body + '}';
    }
}
