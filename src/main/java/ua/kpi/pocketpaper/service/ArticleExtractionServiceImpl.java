package ua.kpi.pocketpaper.service;

import com.gotoquiz.webcoding.TitleExtractor;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.persistence.dao.ArticleDao;
import ua.kpi.pocketpaper.persistence.entity.Article;

import javax.inject.Inject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ArticleExtractionServiceImpl implements ArticleExtractionService {

    private static final Logger logger = LoggerFactory.getLogger(ArticleExtractionService.class);

    private ArticleDao articleDao;

    @Inject
    public ArticleExtractionServiceImpl(ArticleDao articleDao) {
        this.articleDao = articleDao;
    }

    @Override
    public Article createArticle(String url) {
        Article article = new Article();
        article.setTitle(extractTitle(url));
        article.setBody(extractArticle(url));

        articleDao.save(article);

        logger.debug("Created article from URl=[{}]", url);

        return article;
    }

    @Override
    public String extractTitle(String url) {
        try {
            return TitleExtractor.getPageTitle(url);
        } catch (IOException e) {
            logger.error("Failed to extract article title", e);
            throw new BusinessException("Failed to extract article", e);
        }
    }

    @Override
    public String extractArticle(String url) {
        try {
            return ArticleExtractor.getInstance().getText(new URL(url));
        } catch (BoilerpipeProcessingException e) {
            logger.error("Failed to extract article body", e);
            throw new BusinessException("Failed to extract article", e);
        } catch (MalformedURLException e) {
            logger.error("Invalid web page URL", e);
            throw new BusinessException("Invalid web page URL", e);
        }
    }
}
