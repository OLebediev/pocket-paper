package ua.kpi.pocketpaper.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.pocketpaper.persistence.dao.ArticleDao;
import ua.kpi.pocketpaper.persistence.dao.UserDao;
import ua.kpi.pocketpaper.persistence.entity.Article;
import ua.kpi.pocketpaper.persistence.entity.User;
import ua.kpi.pocketpaper.security.MessageDigestProvider;

import javax.inject.Inject;
import java.security.MessageDigest;
import java.util.List;

public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserDao userDao;
    private ArticleDao articleDao;
    private ArticleExtractionService articleExtractionService;
    private MessageDigestProvider digestProvider;

    @Inject
    public UserServiceImpl(UserDao userDao, ArticleDao articleDao, ArticleExtractionService articleExtractionService, MessageDigestProvider digestProvider) {
        this.userDao = userDao;
        this.articleDao = articleDao;
        this.articleExtractionService = articleExtractionService;
        this.digestProvider = digestProvider;
    }

    @Override
    public User getUser(Object userId) {
        return userDao.findById(userId);
    }

    @Override
    public Article getArticle(Object articleId) {
        return articleDao.findById(articleId);
    }

    @Override
    public List<Article> getArticles(Object userId) {
        return articleDao.findAllByUserId(userId);
    }

    @Override
    public void saveArticle(User user, String articleUrl) {
        Article article = articleExtractionService.createArticle(articleUrl);
        article.setUserId(user.getUserId());
        articleDao.save(article);

        logger.debug("Saved article=[{}] for user=[{}]", articleUrl, user.getUsername());
    }

    @Override
    public void addUser(User user, String password) {
        if (userDao.findByUsername(user.getUsername()) != null) {
            throw new BusinessException("Username already exists");
        }

        MessageDigest messageDigest = digestProvider.get();
        messageDigest.update(password.getBytes());
        user.setPasswordHash(messageDigest.digest());
        userDao.save(user);

        logger.info("Added new user user=[{}]", user.getUsername());
    }
}
