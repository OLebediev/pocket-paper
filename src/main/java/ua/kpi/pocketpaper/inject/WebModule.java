package ua.kpi.pocketpaper.inject;

import com.google.inject.Scopes;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import ua.kpi.pocketpaper.web.exception.SecurityExceptionMapper;
import ua.kpi.pocketpaper.web.resource.ArticleResource;
import ua.kpi.pocketpaper.web.resource.TokenResource;
import ua.kpi.pocketpaper.web.resource.UserResource;

import java.util.HashMap;
import java.util.Map;

public class WebModule extends ServletModule {

    @Override
    protected void configureServlets() {
        bind(UserResource.class);
        bind(TokenResource.class);
        bind(ArticleResource.class);
        bind(SecurityExceptionMapper.class);
        bind(JacksonJsonProvider.class).in(Scopes.SINGLETON);

        serve("/*").with(GuiceContainer.class, getJerseyInitParams());
    }

    private static Map<String, String> getJerseyInitParams() {
        Map<String, String> jerseyParams = new HashMap<>();
        jerseyParams.put("com.sun.jersey.spi.container.ResourceFilters", "ua.kpi.pocketpaper.web.DefaultResourceFilterFactory");
        return jerseyParams;
    }
}
