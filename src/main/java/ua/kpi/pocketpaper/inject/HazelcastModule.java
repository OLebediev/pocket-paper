package ua.kpi.pocketpaper.inject;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

public class HazelcastModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HazelcastInstance.class).toProvider(new Provider<HazelcastInstance>() {
            @Override
            public HazelcastInstance get() {
                Config config = new Config();
                return Hazelcast.newHazelcastInstance(config);
            }
        });
    }
}
