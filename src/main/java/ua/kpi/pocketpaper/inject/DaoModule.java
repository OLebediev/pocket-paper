package ua.kpi.pocketpaper.inject;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import ua.kpi.pocketpaper.persistence.MongoConnectionProvider;
import ua.kpi.pocketpaper.persistence.dao.*;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class DaoModule extends AbstractModule {

    @Override
    protected void configure() {
        try {
            Properties properties = new Properties();
            properties.load(new FileReader("mongo.properties"));
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        bind(UserDao.class).to(UserDaoImpl.class);
        bind(ArticleDao.class).to(ArticleDaoImpl.class);
        bind(MongoConnectionProvider.class);
    }
}
