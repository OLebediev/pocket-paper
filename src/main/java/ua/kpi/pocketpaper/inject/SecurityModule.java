package ua.kpi.pocketpaper.inject;

import com.google.inject.AbstractModule;
import ua.kpi.pocketpaper.security.*;

public class SecurityModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(AuthorizationService.class).to(DefaultAuthorizationService.class);
        bind(TokenProvider.class).to(SimpleTokenProvider.class);
        bind(TokenStore.class).to(DistributedInMemoryTokenStore.class);
        bind(MessageDigestProvider.class);
    }
}
