(function ($) {

	$(document).ready(function () {
		if (!TokenManager.get()) {
			changeView('sign-in')
		} else {
			changeView('article-list')
		}
	});

    var changeView = function (viewName, viewParams) {
        var viewBlock = $('<div />').addClass(viewName).html($('.template.' + viewName).html());
        var contentElement = $('.content').empty().append(viewBlock);
        views[viewName].call(contentElement, viewParams);
    };

    var views = {
        'sign-in': function() {
            this.find('form').on('submit', function (e) {
                var form = $(this);
                Client.login(
                    form.find('input[name=username]').val(),
                    form.find('input[name=password]').val(),
                    function (token) {
                        TokenManager.save(token);
                        changeView('article-list');
                    }
                );

                e.preventDefault();
            });
        },

        'sign-up': function () {

        },

        'article-list': function () {
            var self = this;
            var articleElementHtml = '<li><a href="javascript:;"></a></li>';

            this.find('.btn-refresh').on('click', function () {
                loadArticles();
            });

            this.find('.articles').on('click', 'a', function () {
                changeView('article', {
                    articleId: $(this).attr('data-id')
                });
            });

            var loadArticles = function () {
                Client.getArticles(function (articles) {
                    var articlesElement = self.find('.articles').empty();

                    $.each(articles, function () {
                        var articleElement = $(articleElementHtml);
                        articlesElement.append(articleElement);
                        articleElement.find('a').attr('data-id', this.articleId).text(this.title);
                    });
                });
            };

            loadArticles();
        },

        'article': function (viewParams) {
            var self = this;

            this.find('.btn-back').on('click', function () {
                changeView('article-list')
            });

            var loadArticle = function () {
                Client.getArticle(viewParams.articleId, function (article) {
                    self.find('.title').html(article.title);
                    self.find('.body').html(article.body);
                });
            };

            loadArticle();
        }
    };
})(jQuery);

(function ($) {
	var url = 'http://127.0.0.1:8000/rest';

	var Client = {};

	Client.login = function (username, password, callback) {
		$.ajax({
			url: url + '/token/' + username + '/' + password,
			success: function (data) {
				callback(data.token);
			}
		});
	};

	Client.getArticles = function (callback) {
		$.ajax({
			url: url + '/article/all',
			headers: {
				'Authorization': 'Bearer ' + TokenManager.get()
			},
			success: function (data) {
				callback(data);
			}
		});
	};

	Client.getArticle = function (articleId, callback) {
		$.ajax({
			url: url + '/article/' + articleId,
			headers: {
				'Authorization': 'Bearer ' + TokenManager.get()
			},
			success: function (data) {
				callback(data);
			}
		});
	};

	window.Client = Client;
})(jQuery);

(function () {
	var TokenManager = {};
	
	TokenManager.get = function () {
		return localStorage.getItem('token');
	};

	TokenManager.save = function (token) {
		localStorage.setItem('token', token);
	};

	window.TokenManager = TokenManager;
})();
