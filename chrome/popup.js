(function($) {

	$(document).ready(function () {
		$('.login-form').on('submit', function (e) {
			var form = $(this);

			changeContentBlock('loading');
			
			Client.login(
				form.find('input[name=username]').val(), 
				form.find('input[name=password]').val(),
				function () {
					changeContentBlock('done');
				}
			);

			e.preventDefault();
		});

		// chrome.storage.local.clear();

		Client.isLoggedIn(function (loggedIn) {
			if (!loggedIn) {
				changeContentBlock('login-form');
			} else {
				changeContentBlock('loading');
				currentUrl(function (url) {
			        Client.saveArticle(url, function () {
						changeContentBlock('done');
			        });
				});
			}
		});
	});

	var changeContentBlock = function (name) {
		$('.content-block').hide();
		$('.content-block.' + name).show();
	}	

	var currentUrl = function (callback) {
		chrome.tabs.query({ 'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT }, function (tabs) {
            callback(tabs[0].url);            
		});
	};
})(jQuery);

(function ($) {
	var url = 'http://127.0.0.1:8000/rest';

	var Client = {};

	Client.isLoggedIn = function (callback) {
		TokenManager.get(function (token) {
			callback(Boolean(token));
		});
	};

	Client.login = function (username, password, callback) {
		$.ajax({
			url: url + '/token/' + username + '/' + password,
			method: 'GET',
			success: function (data) {
				TokenManager.save(data.token, function () {
					callback();
				});
			}
		});
	};

	Client.saveArticle = function (articleUrl, callback) {
		TokenManager.get(function (token) {
			$.ajax({
				url: url + '/article/' + encodeURIComponent(articleUrl),
				method: 'POST',
				dataType: 'html',
				headers: {
					'Authorization': 'Bearer ' + token
				},
				success: function () {
					callback();
				}
			});
		});
	}

	window.Client = Client;
})(jQuery);

(function () {
	var TokenManager = {};

	TokenManager.save = function (token, callback) {
		chrome.storage.local.set({
			token: token
		}, function () {
			callback();
		});
	};

	TokenManager.get = function (callback) {
		chrome.storage.local.get(['token'], function (token) {
			callback(token.token);
		});
	};

	window.TokenManager = TokenManager;
})();
